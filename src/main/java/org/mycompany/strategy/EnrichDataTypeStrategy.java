package org.mycompany.strategy;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.mycompany.entity.FtpAccountEntity;

public class EnrichDataTypeStrategy implements AggregationStrategy{

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		// TODO Auto-generated method stub
		if(newExchange == null) {
			return oldExchange;
		}
		FtpAccountEntity account = newExchange.getMessage().getBody(FtpAccountEntity.class);
		if(account == null) {
			throw new RuntimeException("Folder not management");
		}
		oldExchange.getMessage().setHeader("dataType", account.getDataType());
		return oldExchange;
	}

}
