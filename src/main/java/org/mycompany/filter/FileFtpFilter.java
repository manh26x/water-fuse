package org.mycompany.filter;

import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileFilter;

public class FileFtpFilter<T> implements GenericFileFilter<T> {

	@Override
	public boolean accept(GenericFile<T> file) {

	    // we only want report files
		return file.getFileName().endsWith(".txt") || file.getFileName().endsWith(".xls") || file.getFileName().endsWith(".xlsx") || ( !file.getFileName().contains(".") && file.isDirectory()) ;
	}

}
